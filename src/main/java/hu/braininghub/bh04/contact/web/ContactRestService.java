package hu.braininghub.bh04.contact.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import hu.braininghub.bh04.contact.model.Contact;
import hu.braininghub.bh04.contact.service.ContactService;

@Path("/contact")
public class ContactRestService {

	@EJB
	private ContactService service;

	@Context
	private UriInfo uriInfo;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getContacts() {

		List<Contact> contacts = service.getAllContact();

		return Response.status(Response.Status.OK).entity(contacts).build();

	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getContactById(@PathParam("id") Integer id) {

		Contact c = service.getContactById(id);

		if (c == null) {
			return Response.status(Status.NOT_FOUND).header("message", "Entity with the specified id not found: " + id)
					.build();
		}
		return Response.status(Response.Status.OK).entity(c).build();

	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getContactById(Contact contact) throws URISyntaxException {

		service.addContact(contact);

		URI location = new URI(uriInfo.getRequestUri() + "/" + contact.getId());
		return Response.status(Response.Status.CREATED).location(location).build();

	}
}
