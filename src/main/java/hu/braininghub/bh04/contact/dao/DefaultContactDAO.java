package hu.braininghub.bh04.contact.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;

import hu.braininghub.bh04.contact.model.Contact;

@Dependent
public class DefaultContactDAO implements ContactDAO {

	private final Map<Integer, Contact> CACHE = new HashMap<Integer, Contact>();

	@PostConstruct
	public void init() {

		Contact c = new Contact();
		c.setEmail("attila.balogh-biro@ui.city");
		c.setFirstName("Attila");
		c.setLastName("Balogh-Biro");
		c.setPhoneNumber("+36709074129");
		c.setId(0);

		CACHE.put(c.getId(), c);

		Contact c2 = new Contact();
		c2.setEmail("horv.daniel@gmail.com");
		c2.setFirstName("Daniel");
		c2.setLastName("Horvath");
		c2.setPhoneNumber("+36204136036");
		c2.setId(1);

		CACHE.put(c2.getId(), c2);
	}

	public void persist(Contact c) {
		CACHE.put(c.getId(), c);
	}

	public Contact getByFirstName(String firstName) {

		for (Contact s : CACHE.values()) {

			if (firstName.equals(s.getFirstName())) {
				return s;
			}
		}
		throw new IllegalArgumentException("Contact not found with firstname ->" + firstName);
	}

	public List<Contact> getAll() {
		return new ArrayList<Contact>(CACHE.values());
	}

	@Override
	public Contact getContactById(Integer id) {
		return CACHE.get(id);
	}

}
