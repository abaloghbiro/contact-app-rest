package hu.braininghub.bh04.contact.service;

import java.util.List;

import javax.ejb.Local;

import hu.braininghub.bh04.contact.model.Contact;

@Local
public interface ContactService {

	List<Contact> getAllContact();
	
	Contact contactByFirstName(String firstName);
	
	void addContact(Contact c);
	
	Contact getContactById(Integer id);
}
