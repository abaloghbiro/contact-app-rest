package hu.braininghub.bh04.contact.service;

import java.util.List;

import javax.ejb.Remote;

import hu.braininghub.bh04.contact.model.Contact;

@Remote
public interface RemoteContactService {

	List<Contact> getAllContact();

	Contact contactByFirstName(String firstName);

	void addContact(Contact c);
}
